﻿
using UnityEngine;

namespace Company.NewApp
{
    public class AppCoreBusiness : SimpleUnitySingleton<AppCoreBusiness>
    {
        private void Start()
        {
            Init();
        }

        /// <summary>
        /// 核心业务逻辑入口
        /// </summary>
        public void Init()
        {
            Debug.Log("[AppCoreBusiness] GameStart");
        }
        
    }
}