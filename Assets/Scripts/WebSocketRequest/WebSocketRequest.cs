﻿/*

WebSocket请求类

*/

using BestHTTP.WebSocket;
using System;
using UnityEngine;

namespace Company.WebSocketRequest
{
    public class WebSocketRequest:IWebSocketMessageOperator
    {
        private string m_WebSocketId;

        private WebSocketRequestTask m_Task;

        private WebSocket m_WebSocket;


        public string WebSocketId { get { return m_WebSocketId; } }

        public WebSocketRequestTask Task { get { return m_Task; } }

        public WebSocket WebSocket { get { return m_WebSocket; } }

        public WebSocketRequest(WebSocketRequestTask task)
        {
            if (task == null)
            {
                return;
            }

            m_Task = task;
            m_WebSocketId = task.WebSocketId;
            m_WebSocket = new WebSocket(new Uri(task.Url));
            AddListener(task);
        }

        private void AddListener(WebSocketRequestTask task)
        {
            WebSocket.OnOpen += task.OnOpen;
            WebSocket.OnMessage += task.OnMessage;
            WebSocket.OnBinary += task.OnBinary;
            WebSocket.OnError += task.OnError;
            WebSocket.OnClosed += task.OnClose;
        }

        /// <summary>
        /// 开启WebSocket
        /// </summary>
        public void Open()
        {
            if (WebSocket == null)
            {
                m_WebSocket = new WebSocket(new Uri(m_Task.Url));
            }
            if (!WebSocket.IsOpen)
            {
                WebSocket.Open();
            }
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="message">字符串消息</param>
        public void SendMessage(string message)
        {
            if (WebSocket != null && WebSocket.IsOpen)
            {
                WebSocket.Send(message);
            }
            else
            {
                Debug.LogErrorFormat("[WebSocketRequest] Cannot send message at websocket id:-{0}-", m_Task?.WebSocketId);
            }
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="message">字节数组消息</param>
        public void SendMessage(byte[] buffer)
        {
            if (WebSocket != null && WebSocket.IsOpen)
            {
                WebSocket.Send(buffer);
            }
        }

        /// <summary>
        /// 关闭WebSocket
        /// webSocket实例对象被Close后，该对象便不能再被复用，因而置空
        /// </summary>
        public void Close()
        {
            if (WebSocket != null && WebSocket.IsOpen)
            {
                Debug.LogFormat("[WebSocketRequest] Close at web socket id:-{0}-", m_Task?.WebSocketId);
                WebSocket.Close();
                m_WebSocket = null;
            }
        }
    }
}