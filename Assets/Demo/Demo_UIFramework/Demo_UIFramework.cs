using Company.NewApp.Presenters;
using UnityEngine;

namespace Company.DevFramework.Demo
{
    public class Demo_UIFramework : MonoBehaviour
    {
        [SerializeField] UIExamplePresenter m_UIExamplePresenter;

        private void Start()
        {
            m_UIExamplePresenter.Init();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_UIExamplePresenter.ShowView();
            }
        }
    }
}