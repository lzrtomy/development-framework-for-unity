using UnityEngine;
using Company.NewApp.Views;
using Company.Constants;
using Company.Attributes;

namespace Company.NewApp.Presenters
{
    public class UIExamplePresenter : UIPresenterBase
    {
        [ReadOnly]
        [SerializeField] UIExampleView m_UIExampleView;

        public override void Init(params object[] parameters)
        {
            base.Init(parameters);
        }

        protected override void AddListeners()
        {
            EventManager.Instance.AddListener(MyEventName.Demo_UI.OnCloseUIExample.ToString(), OnClose);
        }

        protected override void RemoveListeners()
        {
            EventManager.Instance.RemoveListener(MyEventName.Demo_UI.OnCloseUIExample.ToString(), OnClose);
        }

        public void ShowView()
        {
            if (!m_UIExampleView)
            {
                m_UIExampleView = UIManager.Instance.Open<UIExampleView>(ViewType.Example, "UIExample", transform);
            }
        }

        protected override void OnClose()
        {
            CloseViewPrefab(m_UIExampleView);
            m_UIExampleView = null;
        }
    }
}